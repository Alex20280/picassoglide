package layouts.sourceit.com.picassoglide.image;


import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import layouts.sourceit.com.picassoglide.R;

public class ImageDownloader implements ImageInteractor {

    private Context context;

    public ImageDownloader(Context context) {
        this.context = context;
    }

    @Override
    public void load(@Flag int flag, String imgUrl, ImageView target) {
        switch (flag) {
            case Flag.PICASSO:
                loadWithPicasso(imgUrl, target);
                break;
            case Flag.GLIDE:
                loadWithGlide(imgUrl, target);
                break;
        }
    }

    private void loadWithPicasso(String imgUrl, ImageView target) {
        Picasso.with(context)
                .load(imgUrl)
                .resize(500, 500)
                .placeholder(R.mipmap.ic_launcher)
                .into(target);
    }

    private void loadWithGlide(String imgUrl, ImageView target) {
        Glide.with(context)
                .load(imgUrl)
                .override(500, 500)
                .crossFade()
                .placeholder(R.mipmap.ic_launcher)
                .into(target);
    }
}
