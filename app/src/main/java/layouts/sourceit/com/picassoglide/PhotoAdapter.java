package layouts.sourceit.com.picassoglide;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import layouts.sourceit.com.picassoglide.image.ImageDownloader;
import layouts.sourceit.com.picassoglide.image.ImageInteractor;

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.PhotoHolder> {


    private List <String> photos;
    private ImageDownloader imageDownloader;

    public PhotoAdapter(Context context, List<String> photos) {
        this.photos = photos;
        imageDownloader = new ImageDownloader(context);
    }

    @Override
    public PhotoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.photo_item, parent, false);
        return new PhotoHolder(v);
    }

    @Override
    public void onBindViewHolder(PhotoHolder holder, int position) {
        String imgUrl = photos.get(position);
        imageDownloader.load(ImageInteractor.Flag.PICASSO, imgUrl,holder.photo);
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public class PhotoHolder extends RecyclerView.ViewHolder {

        ImageView photo;

        public PhotoHolder(View itemView) {
            super(itemView);

            photo = (ImageView) itemView.findViewById(R.id.photo_item);
        }
    }
}
