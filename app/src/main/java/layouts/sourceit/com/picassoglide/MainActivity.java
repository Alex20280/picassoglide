package layouts.sourceit.com.picassoglide;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static List<String> photoItem = new ArrayList<>();

    static {
        for (int i =0; i<10; i++){
            photoItem.add("http://www1.pictures.zimbio.com/gi/Elon+Musk+Wr9KinBO5Fqm.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461930513.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461931114.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461931189.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461931350.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461931384.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461933353.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461933416.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461933508.jpg");
//            photoItem.add("https://uznayvse.ru/images/stories2016/uzn_1461933353.jpg");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PhotoAdapter photoAdapter = new PhotoAdapter(this, photoItem);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.photos);

        if (recyclerView != null){
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(photoAdapter);
        }

    }
}
